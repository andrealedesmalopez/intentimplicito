package com.example.intentimplicito;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import static android.content.Intent.ACTION_CALL;

public class Implicito extends AppCompatActivity {

    private EditText editNumero;
    private EditText editExplorador;
    private ImageButton botonNumero;
    private ImageButton botonExplorador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implicito);
        editNumero=(EditText)findViewById(R.id.Numero);
        editExplorador=(EditText)findViewById(R.id.Explorador);

        botonNumero=(ImageButton)findViewById(R.id.btnNumero);
        botonExplorador=(ImageButton)findViewById(R.id.btnExplorador);


        botonNumero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num= editNumero.getText().toString();
                if (num!=null){
                    //Comprobando version actual de android
                    //versión actual>= version6
                    if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
                        //versionNueva();
                        //pedir los permisos hasta el momento en que se utilicen
                    }else{
                        //diferencia en permisos
                        //permisos pedidos desde su instalación
                        versionesAnteriores(num);

                    }
                }
            }

            private void versionesAnteriores(String num){
                Intent intentLlamada = new Intent(ACTION_CALL, Uri.parse("tel:"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)) {
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(Implicito.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    private boolean verificarPermisos(String permiso){
        int resultado=this.checkCallingOrSelfPermission(permiso);
        return resultado== PackageManager.PERMISSION_GRANTED;
    }
}